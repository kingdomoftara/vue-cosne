import Vue from 'vue'
import Vuex from 'vuex'
import { events } from './events.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    calendar: [
      { name: 'january', days: 31 },
      { name: 'february', days: 28 },
      { name: 'march', days: 31 },
      { name: 'april', days: 30 },
      { name: 'may', days: 31 },
      { name: 'june', days: 30 },
      { name: 'july', days: 31 },
      { name: 'august', days: 31 },
      { name: 'september', days: 30 },
      { name: 'october', days: 31 },
      { name: 'november', days: 30 },
      { name: 'december', days: 31 }
    ],
    events
  },
  getters: {
    getDaysInMonth: state => month => {
      return state.calendar.find(calendarMonth => calendarMonth.name === month).days
    },
    hasEvents: state => (date, month) => {
      let result = false
      state.events[month].forEach(event => {
        if (event.date == date) {
          result = true
        }
      })
      return result
    },
    getEvents: state => (month_name, date) => {
      let month = state.events[month_name]
      let events = month.filter(function (key) {
        return key['date'] == date
      })
      let result_array = Object.keys(events).map(key => {
        return events[key]
      })
      return result_array
    },
    getSurroundingMonths: state => month => {
      var result = {
        previousMonth: null,
        followingMonth: null
      }
      var monthIndex = state.calendar.findIndex(
        calendarMonth => calendarMonth.name == month
      )

      if (monthIndex == 0) {
        result.previousMonth = state.calendar[state.calendar.length - 1].name
      } else result.previousMonth = state.calendar[(monthIndex - 1)].name

      if (monthIndex == state.calendar.length - 1) {
        result.followingMonth = state.calendar[0].name;
      } else result.followingMonth = state.calendar[(monthIndex + 1)].name

      return result;
    }
  },
  mutations: {
    mutateToLeapYear (state) {
      let february = state.calendar.find(month => month.name == 'february')
      february.days = 29
    }
  },
  actions: {
    updateToLeapYear ({ commit }) {
      commit('mutateToLeapYear')
    }
  }
})
