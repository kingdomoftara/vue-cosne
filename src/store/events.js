export const events = {
  january: [
    { title: 'spectacle', location: "l'icosne", date: 5, time: "20h" },
    {title: 'concert', location: 'la mairie', date: 5, time: "21h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 5, time: "19h"},
    { title: 'spectacle', location: 'la mairie', date: 18, time: "19h" },
    { title: 'spectacle', location: 'café culturel', date: 18, time: "17h" },
    { title: 'concert', location: 'café culturel', date: 28, time: "10h" }
  ],
  february: [
{ title: 'spectacle', location: "l'icosne", date: 1, time: "20h" },
{ title: 'rendezvous', location: "au tribunal", date: 1, time: "17h" },
    {title: 'concert', location: 'la mairie', date: 6, time: "21h"},
    {title: 'spectacle', location: 'le square', date: 6, time: "18h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 16, time: "19h"},
    { title: 'spectacle', location: 'la mairie', date: 20, time: "19h" },
    { title: 'spectacle', location: 'le paris', date: 20, time: "16h" },
    { title: 'concert', location: 'la mairie', date: 20, time: "15h" },
    { title: 'concert', location: 'café culturel', date: 14, time: "10h" }
  ],
  march: [
    { title: 'spectacle ', location: 'chez amy', date: 27, time: "9h" },
        { title: 'concert ', location: 'au tribunal', date: 18, time: "16h" },
        { title: 'concert ', location: 'au centre social', date: 18, time: "12h" },
        { title: 'concert ', location: 'chez amy', date: 18, time: "9h" },
    { title: 'concert ', location: 'au centre social', date: 4, time: "12h" }
    ],
  april: [
        { title: 'concert ', location: 'au tribunal', date: 18, time: "16h" },
        { title: 'tricotez ensemble ', location: 'au tribunal', date: 18, time: "12h" },
    { title: 'concert ', location: 'au centre social', date: 4, time: "12h" },
    { title: 'concert ', location: 'au centre social', date: 12, time: "12h" },
    { title: 'un thé ', location: 'au centre social', date: 12, time: "10h" }
  ],
  may: [
        { title: 'concert ', location: 'au tribunal', date: 15, time: "16h" },
        { title: 'thé ensemble ', location: 'café culturel', date: 15, time: "10h" },
        { title: 'concert ', location: 'le square', date: 15, time: "14h" },
    { title: 'concert ', location: 'au centre social', date: 20, time: "12h" }
  ],
  june: [
    { title: 'concert ', location: 'au tribunal', date: 18, time: "16h" },
    { title: 'parlons anglais ', location: 'la mairie', date: 18, time: "10h" },
    { title: 'concert ', location: 'au centre social', date: 4, time: "12h" },
    { title: 'thé dansant ', location: 'le m', date: 4, time: "13h" },
    { title: 'spectacle ', location: 'au tribunal', date: 4, time: "10h" },
    { title: 'tricotez! ', location: 'au centre social', date: 4, time: "9h" },
    { title: 'thé ensemble ', location: 'café culturel', date: 4, time: "11h" }
  ],
  july: [{ title: 'spectacle', location: "l'icosne", date: 1, time: "20h" },
    {title: 'concert', location: 'la mairie', date: 6, time: "21h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 16, time: "13h"},
    {title: 'café ensemble', location: 'le square', date: 16, time: "12h"},
    {title: 'parlons allemand', location: 'la médiathèque', date: 16, time: "10h"},
    {title: 'concert', location: 'le m', date: 16, time: "19h"},
    { title: 'spectacle', location: 'la mairie', date: 20, time: "19h" },
    { title: 'concert', location: 'café culturel', date: 14, time: "10h" },
    { title: 'concert', location: 'chez amy', date: 14, time: "17h" },
    ],
  august: [
    { title: 'spectacle', location: "l'icosne", date: 4, time: "20h" },
    { title: 'les jeunes retraités ensemble', location: "la médiathèque", date: 4, time: "18h" },
    { title: 'concert', location: "chez amy", date: 4, time: "20h" },
    {title: 'concert', location: 'la mairie', date: 8, time: "21h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 12, time: "19h"},
    { title: 'spectacle', location: 'la mairie', date: 16, time: "19h" },
    { title: 'les guingettes', location: 'centre social', date: 16, time: "13h" },
    { title: 'concert', location: 'café culturel', date: 22, time: "10h" },
    { title: 'cours anglais', location: 'centre social', date: 22, time: "18h" }
  ],
  september: [
    { title: 'spectacle', location: "l'icosne", date: 5, time: "20h" },
    { title: 'concert', location: "chez amy", date: 5, time: "17h" },
    {title: 'concert', location: 'la mairie', date: 10, time: "21h"},
    {title: 'cours anglais', location: 'centre social', date: 15, time: "10h"},
    {title: 'cours allemand', location: 'centre social', date: 15, time: "11h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 15, time: "19h"},
    { title: 'spectacle', location: 'la mairie', date: 20, time: "19h" },
    { title: 'les jeunes retraités ensemble', location: 'la médiathèque', date: 20, time: "18h" },
    { title: 'concert', location: 'café culturel', date: 25, time: "10h" }
  ],
  october: [
    { title: 'spectacle', location: "l'icosne", date: 1, time: "20h" },
    {title: 'concert', location: 'la mairie', date: 7, time: "21h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 14, time: "19h"},
    {title: 'parlons allemand', location: 'chez amy', date: 14, time: "11h"},
    {title: 'spectacle', location: 'le tro\'p', date: 14, time: "16h"},
    {title: 'dansons ensemble', location: 'le m', date: 14, time: "14h"},
    {title: 'thé et conversations', location: 'le square', date: 14, time: "10h"},
    { title: 'spectacle', location: 'la mairie', date: 21, time: "19h" },
    { title: 'concert', location: 'café culturel', date: 28, time: "10h" },
    { title: 'cours anglais', location: 'centre social', date: 28, time: "19h" },
    { title: 'parlons anglais', location: 'chez amy', date: 28, time: "11h" },
  ],
  november: [
    { title: 'spectacle', location: 'les guingettes', date: 20, time: "7h" },
    { title: 'concert', location: 'la mairie', date: 20, time: "18h" },
    { title: 'parlons anglais', location: 'café culturel', date: 20, time: "10h" },
    { title: 'thé dansant ', location: 'dans l\'île', date: 6, time: "16h" },
    { title: 'cours allemand ', location: 'centre social', date: 6, time: "13h" }
  ],
  december: [
    { title: 'spectacle', location: "l'icosne", date: 2, time: "20h" },
    {title: 'concert', location: 'la mairie', date: 14, time: "21h"},
    {title: 'thé dansant', location: 'dans l\'île', date: 22, time: "19h"},
    {title: 'parlons avant noël', location: 'la mairie', date: 22, time: "16h"},
    {title: 'cours anglais', location: 'centre social', date: 22, time: "11h"},
    { title: 'spectacle', location: 'la mairie', date: 19, time: "19h" },
    { title: 'concert', location: 'café culturel', date: 14, time: "10h" }
  ]
}
