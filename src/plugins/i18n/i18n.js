/* eslint-disable */
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { en } from './en.js'
import { fr } from './fr.js'

Vue.use(VueI18n)

const messages = {
        en,          
        fr
}

export const i18n = new VueI18n({
    locale: 'fr',
    fallbackLocale: 'fr',
    messages
})