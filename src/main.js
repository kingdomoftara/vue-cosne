import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

import FlagIcon from 'vue-flag-icon'
import {i18n}  from '@/plugins/i18n/i18n.js'
import {routes} from './routes'
import store from './store/store'

Vue.use(FlagIcon);

Vue.use(VueRouter)
const router = new VueRouter({
  /** problem with Vue application in Gitlab, need to disable history mode*/
  //mode: 'history',
  routes
})

require('@/css/style.css')

Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
