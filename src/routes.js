import Events from './components/Events.vue'
import Home from './components/Home.vue'
import Month from './components/Month.vue'

export const routes = [
    {
        path: '',
        name: 'home',
        component: Home

    },
    {
        path: '/events',
        name: 'events',
        component: Events
    },
    {
        path: '/month/:month',
        name: 'month',
        component: Month,
        props: true
    },
    {
        path: '*',
        redirect: '/'
    }
]