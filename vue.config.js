module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
  ? '/vue-cosne/'
  : '/',
  productionSourceMap: false
}